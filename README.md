# README #


### UI mistakes ###

1 - Dividers is different to each other.  Dividers must must be the same and must have oppasity of either 12 % black fror light theme and 12% white in dark themes and have 1 dp thick ("Material design" pattern )

2 -  The distance between dividers in the area of TextView "Створено" isn't the same that the distance between other dividers

3 - The distance from TextView "Зареєстровано" and TextView with Date to top divider   isn't the same that the distance to bottom divider that's why Views isn't in the middle between dividers

4 - The icon (Arrow) of toolbar home button is not native

### Solutions ###

1 - Use one style for all dividers 

2st and 3th -  use the same margins or padding for Views between dividers  

4 - Use the default icon for toolbar home button