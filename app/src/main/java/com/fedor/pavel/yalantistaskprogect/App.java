package com.fedor.pavel.yalantistaskprogect;

import android.app.Application;
import android.graphics.Bitmap;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;


public class App extends Application {


    public static final int DISK_CACHE_SIZE_FOR_IMAGES = 2 * 1024 * 1024; // - 2 мб
    public static final int MEMORY_CACHE_SIZE_PERCENTAGE_FOR_IMAGES = 8;
    public static final int DISK_CACHE_FILE_COUNT_FOR_IMAGES = 100;

    @Override
    public void onCreate() {
        super.onCreate();
        configureImageLoader();
    }

    private void configureImageLoader() {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .diskCacheFileCount(DISK_CACHE_FILE_COUNT_FOR_IMAGES)
                .diskCacheSize(DISK_CACHE_SIZE_FOR_IMAGES)
                .memoryCacheSizePercentage(MEMORY_CACHE_SIZE_PERCENTAGE_FOR_IMAGES)
                .defaultDisplayImageOptions(configCustomDisplayImageOptions())
                .build();

        ImageLoader.getInstance().init(config);
    }

    public DisplayImageOptions configCustomDisplayImageOptions(){
        return new DisplayImageOptions.Builder()
                .bitmapConfig(Bitmap.Config.RGB_565)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .build();
    }


}
