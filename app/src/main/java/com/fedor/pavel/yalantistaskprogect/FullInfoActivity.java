package com.fedor.pavel.yalantistaskprogect;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.fedor.pavel.yalantistaskprogect.adapters.RVPhotosAdapter;
import com.fedor.pavel.yalantistaskprogect.models.ClaimModel;
import com.fedor.pavel.yalantistaskprogect.views.RVPhotoHorizontalDivider;

import butterknife.Bind;
import butterknife.ButterKnife;

public class FullInfoActivity extends AppCompatActivity {

    @Bind(R.id.activity_full_info_rvPhotos)
    RecyclerView mRvPhotos;

    @Bind(R.id.activity_full_info_tvServices)
    TextView mTvServices;

    @Bind(R.id.activity_full_info_tvCreatedItem)
    TextView mTvCreated;

    @Bind(R.id.activity_full_info_tvExecuteItem)
    TextView mTvExecute;

    @Bind(R.id.activity_full_info_tvChargeItem)
    TextView mTvCharge;

    @Bind(R.id.activity_full_info_tvRegItem)
    TextView mTvRegistered;

    @Bind(R.id.activity_full_info_tvIndicator)
    TextView mTvIndicator;

    @Bind(R.id.activity_full_info_tvDescription)
    TextView mTvDescription;

    private ClaimModel mClaimModel;
    private RVPhotosAdapter mAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_info);

        ButterKnife.bind(this);
        loadClaim();
        fillViews();
        prepareRecyclerView();
        prepareToolbar();
    }

    private void fillViews() {
        mTvServices.setText(mClaimModel.getServices());
        mTvCreated.setText(mClaimModel.getCreatedDate());
        mTvRegistered.setText(mClaimModel.getRegisteredDate());
        mTvCharge.setText(mClaimModel.getInCharge());
        mTvExecute.setText(mClaimModel.getExecuteUntil());
        mTvDescription.setText(mClaimModel.getDescription());
        mTvIndicator.setText(mClaimModel.getWorkStatus());
    }

    private void prepareRecyclerView() {
        mAdapter = new RVPhotosAdapter(this, mClaimModel.getAllURLs());
        mRvPhotos.setAdapter(mAdapter);
        mRvPhotos.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mRvPhotos.addItemDecoration(new RVPhotoHorizontalDivider(this, R.dimen.activity_horizontal_margin));
    }

    private void prepareToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(mClaimModel.getTitle());
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void loadClaim() {
        mClaimModel = new ClaimModel(getResources().getText(R.string.claimName).toString()
                , getResources().getText(R.string.servicesName).toString()
                , getResources().getText(R.string.itemCreatedContentText).toString()
                , getResources().getText(R.string.itemRegContentText).toString()
                , getResources().getText(R.string.itemExecuteContentText).toString()
                , getResources().getText(R.string.itemChargeContentText).toString()
                , getResources().getText(R.string.description).toString()
                , getResources().getText(R.string.inWorkProgressIndicatorTitle).toString()
                , getResources().getStringArray(R.array.urls));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View v) { //  I made such method for grouping TextViews by click event type
        switch (v.getId()) {
            case R.id.activity_full_info_tvServices:
            case R.id.activity_full_info_tvCreatedItem:
            case R.id.activity_full_info_tvExecuteItem:
            case R.id.activity_full_info_tvChargeItem:
            case R.id.activity_full_info_tvRegItem:
            case R.id.activity_full_info_tvIndicator:
            case R.id.activity_full_info_tvChargeTitle:
            case R.id.activity_full_info_tvCreatedTitle:
            case R.id.activity_full_info_tvExecuteTitle:
            case R.id.activity_full_info_tvRegTitle:
            case R.id.activity_full_info_tvDescription:

                Snackbar.make(v, v.getClass().getSimpleName(), Snackbar.LENGTH_SHORT).show();
                break;
        }
    }

}
