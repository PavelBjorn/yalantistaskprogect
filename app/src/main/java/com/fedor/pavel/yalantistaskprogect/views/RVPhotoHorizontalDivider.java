package com.fedor.pavel.yalantistaskprogect.views;


import android.content.Context;

import android.graphics.Rect;
import android.support.annotation.DimenRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class RVPhotoHorizontalDivider extends RecyclerView.ItemDecoration {


    private int mItemOffsetRight;
    private int mItemOffsetBottom = 0;
    private int mItemOffsetTop = 0;
    private int mItemOffsetLeft = 0;


    public RVPhotoHorizontalDivider(Context context, @DimenRes int itemOffsetRightResDimen) {
        this.mItemOffsetRight = context.getResources().getDimensionPixelSize(itemOffsetRightResDimen);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if ((parent.getAdapter().getItemCount() - 1) != parent.getChildAdapterPosition(view)) {
            outRect.set(mItemOffsetLeft, mItemOffsetTop, mItemOffsetRight, mItemOffsetBottom);
        }
    }
}
