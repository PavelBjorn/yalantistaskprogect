package com.fedor.pavel.yalantistaskprogect.adapters;


import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fedor.pavel.yalantistaskprogect.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RVPhotosAdapter extends RecyclerView.Adapter<RVPhotosAdapter.PhotosViewHolder> {

    private List<String> mPhotosURL;
    private LayoutInflater mInflater;

    public RVPhotosAdapter(Context context, List<String> photosURL) {

        mInflater = LayoutInflater.from(context);

        this.mPhotosURL = photosURL;

    }

    public RVPhotosAdapter(Context context) {

        mInflater = LayoutInflater.from(context);
        mPhotosURL = new ArrayList<>();

    }

    public void addPhotosURL(String url) {

        mPhotosURL.add(url);

    }

    public void addAllPhorosURL(Collection <String> photosURL) {

        int startPosition = this.mPhotosURL.size() - 1;

        this.mPhotosURL.addAll(photosURL);

        notifyItemRangeInserted(startPosition, photosURL.size());

    }

    public String getURL(int position) throws IndexOutOfBoundsException {

        return mPhotosURL.get(position);

    }

    public ArrayList<String> getAllURLs() {

        return new ArrayList<>(mPhotosURL);

    }

    public void remove(int position) throws IndexOutOfBoundsException {

        mPhotosURL.remove(position);

    }

    public void clear() {

        this.mPhotosURL.clear();

        notifyItemRangeRemoved(0, mPhotosURL.size());

    }

    @Override
    public PhotosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new PhotosViewHolder(mInflater.inflate(R.layout.item_rv_photos, parent, false));

    }

    @Override
    public void onBindViewHolder(PhotosViewHolder holder, int position) {

        ImageLoader.getInstance().displayImage(mPhotosURL.get(position), holder.imvPhoto);

    }

    @Override
    public int getItemCount() {
        return mPhotosURL.size();
    }

    class PhotosViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.item_rv_photos_imvPhoto)
        ImageView imvPhoto;

        public PhotosViewHolder(View itemView) {

            super(itemView);

            ButterKnife.bind(this, itemView);

            imvPhoto.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.item_rv_photos_imvPhoto:

                    Snackbar.make(itemView, "ImageView", Snackbar.LENGTH_SHORT).show();

                    break;

            }

        }

    }

}
