package com.fedor.pavel.yalantistaskprogect.models;


import java.util.ArrayList;
import java.util.Arrays;

public class ClaimModel {

    private String mTitle;
    private String mServices;
    private String mCreatedDate;
    private String mRegisteredDate;
    private String mExecuteUntil;
    private String mInCharge;
    private String mDescription;
    private String mWorkStatus;
    private ArrayList<String> mPhotosURL = new ArrayList<>();

    public ClaimModel() {

    }

    public ClaimModel(String title
            , String services
            , String createdDate
            , String registeredDate
            , String executeUntil
            , String inCharge
            , String description
            , String workStatus
            , ArrayList<String> photosURL) {

        this.mCreatedDate = createdDate;
        this.mRegisteredDate = registeredDate;
        this.mExecuteUntil = executeUntil;
        this.mInCharge = inCharge;
        addAllPhotosURL(photosURL);
        this.mTitle = title;
        this.mServices = services;
        this.mDescription = description;
        this.mWorkStatus = workStatus;

    }

    public ClaimModel(String title
            , String services
            , String createdDate
            , String registeredDate
            , String executeUntil
            , String inCharge
            , String description
            , String workStatus
            , String[] photosURL) {

        this.mCreatedDate = createdDate;
        this.mRegisteredDate = registeredDate;
        this.mExecuteUntil = executeUntil;
        this.mInCharge = inCharge;
        this.mTitle = title;
        this.mServices = services;
        this.mDescription = description;
        this.mWorkStatus = workStatus;

        addAllPhotosURL(photosURL);

    }

    public void addPhotosURL(String url) {

        mPhotosURL.add(url);

    }

    public void addAllPhotosURL(ArrayList<String> photosURL) {

        this.mPhotosURL.addAll(photosURL);

    }

    public void addAllPhotosURL(String[] photosURL) {
        this.mPhotosURL.addAll(Arrays.asList(photosURL));
    }

    public String getURL(int position) throws IndexOutOfBoundsException {
        return mPhotosURL.get(position);
    }

    public ArrayList<String> getAllURLs() {
        return new ArrayList<>(mPhotosURL);
    }

    public String getCreatedDate() {
        return mCreatedDate;
    }

    public void setmCreatedDate(String mCreatedDate) {
        this.mCreatedDate = mCreatedDate;
    }

    public String getRegisteredDate() {
        return mRegisteredDate;
    }

    public void setmRegisteredDate(String mRegisteredDate) {
        this.mRegisteredDate = mRegisteredDate;
    }

    public String getExecuteUntil() {
        return mExecuteUntil;
    }

    public void setmExecuteUntil(String mExecuteUntil) {
        this.mExecuteUntil = mExecuteUntil;
    }

    public String getInCharge() {
        return mInCharge;
    }

    public void setInCharge(String inCharge) {
        this.mInCharge = inCharge;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getServices() {
        return mServices;
    }

    public void setServices(String services) {
        this.mServices = services;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public String getWorkStatus() {
        return mWorkStatus;
    }

    public void setWorkStatus(String workStatus) {
        this.mWorkStatus = workStatus;
    }

}
